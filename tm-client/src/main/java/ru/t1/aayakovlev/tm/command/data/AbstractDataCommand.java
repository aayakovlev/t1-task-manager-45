package ru.t1.aayakovlev.tm.command.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.command.AbstractCommand;
import ru.t1.aayakovlev.tm.endpoint.DomainEndpoint;

@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected DomainEndpoint getDomainEndpoint() {
        return serviceLocator.getDomainEndpoint();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

}

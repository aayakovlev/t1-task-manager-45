package ru.t1.aayakovlev.tm.repository.dto;

import ru.t1.aayakovlev.tm.dto.model.SessionDTO;

public interface SessionDTORepository extends ExtendedDTORepository<SessionDTO> {

}

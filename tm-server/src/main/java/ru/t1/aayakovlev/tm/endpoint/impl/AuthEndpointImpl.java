package ru.t1.aayakovlev.tm.endpoint.impl;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.request.UserLoginRequest;
import ru.t1.aayakovlev.tm.dto.request.UserLogoutRequest;
import ru.t1.aayakovlev.tm.dto.request.UserShowProfileRequest;
import ru.t1.aayakovlev.tm.dto.response.UserLoginResponse;
import ru.t1.aayakovlev.tm.dto.response.UserLogoutResponse;
import ru.t1.aayakovlev.tm.dto.response.UserShowProfileResponse;
import ru.t1.aayakovlev.tm.endpoint.AuthEndpoint;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.service.AuthService;
import ru.t1.aayakovlev.tm.service.ServiceLocator;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.aayakovlev.tm.endpoint.AuthEndpoint")
public final class AuthEndpointImpl extends AbstractEndpoint implements AuthEndpoint {

    public AuthEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }
    @NotNull
    private AuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) throws AbstractException {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String token = getAuthService().login(login, password);
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        getAuthService().invalidate(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserShowProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserShowProfileRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @NotNull final UserDTO user = getAuthService().profile(userId);
        return new UserShowProfileResponse(user);
    }

}

package ru.t1.aayakovlev.tm.repository.dto.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.comparator.CreatedComparator;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.repository.dto.UserDTORepository;

import javax.persistence.EntityManager;
import java.util.Comparator;

public final class UserDTORepositoryImpl extends AbstractBaseDTORepository<UserDTO> implements UserDTORepository {

    public UserDTORepositoryImpl(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<UserDTO> getClazz() {
        return UserDTO.class;
    }

    @NotNull
    @Override
    protected String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        return "created";

    }

    @NotNull
    @Override
    public UserDTO create(@NotNull final String login, @NotNull final String password) {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(password);
        return save(user);
    }

    @NotNull
    @Override
    public UserDTO create(@NotNull final String login, @NotNull final String password, @Nullable final String email) {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setEmail(email);
        user.setPasswordHash(password);
        return save(user);
    }

    @NotNull
    @Override
    public UserDTO create(@NotNull final String login, @NotNull final String password, @NotNull final Role role) {
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(role);
        return save(user);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        @NotNull final String query = "from " + getClazz().getSimpleName() + " e " +
                "where e.login = :login";
        return entityManager.createQuery(query, getClazz())
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull final String email) {
        @NotNull final String query = "from " + getClazz().getSimpleName() + " e " +
                "where e.email = :email";
        return entityManager.createQuery(query, getClazz())
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean isLoginExists(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}

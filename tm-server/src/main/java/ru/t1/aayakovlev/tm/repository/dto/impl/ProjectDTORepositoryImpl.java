package ru.t1.aayakovlev.tm.repository.dto.impl;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.comparator.CreatedComparator;
import ru.t1.aayakovlev.tm.comparator.NameComparator;
import ru.t1.aayakovlev.tm.comparator.StatusComparator;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.repository.dto.ProjectDTORepository;

import javax.persistence.EntityManager;
import java.util.Comparator;

public final class ProjectDTORepositoryImpl extends AbstractExtendedDTORepository<ProjectDTO>
        implements ProjectDTORepository {


    public ProjectDTORepositoryImpl(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull Class<ProjectDTO> getClazz() {
        return ProjectDTO.class;
    }

    @Override
    @NotNull
    protected String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        if (comparator == StatusComparator.INSTANCE) return "status";
        return "created";
    }

    @NotNull
    @Override
    public ProjectDTO create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        return save(project);
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return save(project);
    }

}

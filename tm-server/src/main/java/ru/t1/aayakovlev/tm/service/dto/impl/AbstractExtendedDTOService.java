package ru.t1.aayakovlev.tm.service.dto.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.AbstractExtendedModelDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.repository.dto.ExtendedDTORepository;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.dto.ExtendedDTOService;

import javax.persistence.EntityManager;
import java.util.*;

public abstract class AbstractExtendedDTOService<E extends AbstractExtendedModelDTO, R extends ExtendedDTORepository<E>>
        extends AbstractBaseDTOService<E, R> implements ExtendedDTOService<E> {

    public AbstractExtendedDTOService(@NotNull final ConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public E save(@Nullable final String userId, @Nullable final E model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (model == null) throw new EntityNotFoundException();
        @Nullable E resultEntity;
        final @NotNull EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            model.setUserId(userId);
            resultEntity = entityRepository.save(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultEntity;
    }

    @NotNull
    @Override
    public Collection<E> add(@Nullable final String userId, @Nullable final Collection<E> models)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<E> resultEntities = new ArrayList<>();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            for (@NotNull final E entity : models) {
                entity.setUserId(userId);
                @NotNull final E resultEntity = entityRepository.save(entity);
                resultEntities.add(resultEntity);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultEntities;
    }

    @Override
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            entityRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int count(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        @NotNull final EntityManager entityManager = getEntityManager();
        int result;
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            result = entityRepository.count(userId);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        try {
            findById(userId, id);
        } catch (EntityNotFoundException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        @Nullable List<E> resultEntities;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            resultEntities = entityRepository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return resultEntities;
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable final Comparator<E> comparator)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (comparator == null) return findAll(userId);
        @Nullable List<E> resultEntities;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            resultEntities = entityRepository.findAll(userId, comparator);
        } finally {
            entityManager.close();
        }
        return resultEntities;
    }

    @NotNull
    @Override
    public E findById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable E resultEntity;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            resultEntity = entityRepository.findById(userId, id);
        } finally {
            entityManager.close();
        }
        if (resultEntity == null) throw new EntityNotFoundException();
        return resultEntity;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final E model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(userId, id)) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final R entityRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            entityRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public E update(@Nullable final String userId, @Nullable final E model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(userId, model.getId())) throw new EntityNotFoundException();
        return update(model);
    }

}

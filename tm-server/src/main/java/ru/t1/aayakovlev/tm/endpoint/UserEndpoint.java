package ru.t1.aayakovlev.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface UserEndpoint extends BaseEndpoint {

    @NotNull
    String NAME = "UserEndpointImpl";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static UserEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static UserEndpoint newInstance(@NotNull final ConnectionProvider connectionProvider) {
        return BaseEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, UserEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static UserEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return BaseEndpoint.newInstance(host, port, NAME, SPACE, PART, UserEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserChangePasswordResponse changePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    UserLockResponse lock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    UserRegistryResponse registry(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request
    ) throws Exception;

    @NotNull
    @WebMethod
    UserRemoveResponse remove(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    UserUnlockResponse unlock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    UserUpdateProfileResponse update(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request
    ) throws AbstractException;

}

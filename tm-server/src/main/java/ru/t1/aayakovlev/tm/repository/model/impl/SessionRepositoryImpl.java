package ru.t1.aayakovlev.tm.repository.model.impl;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.comparator.CreatedComparator;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;
import java.util.Comparator;

public final class SessionRepositoryImpl extends AbstractExtendedRepository<Session> implements SessionRepository {

    public SessionRepositoryImpl(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected @NotNull Class<Session> getClazz() {
        return Session.class;
    }

    @Override
    protected @NotNull String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        return "created";

    }

}

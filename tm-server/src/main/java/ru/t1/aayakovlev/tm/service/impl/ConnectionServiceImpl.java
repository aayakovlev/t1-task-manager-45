package ru.t1.aayakovlev.tm.service.impl;

import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.dto.UserDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.impl.UserDTORepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.DatabaseProperty;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

import static org.hibernate.cfg.Environment.*;

public final class ConnectionServiceImpl implements ConnectionService {

    // TODO: 28.11.2022 добавить работу с h2 для тестов

    @NotNull
    private final DatabaseProperty databaseProperty;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionServiceImpl(@NotNull final DatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.entityManagerFactory = factory();
        initAdminUser();
    }

    @NotNull
    @Override
    @SneakyThrows
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(URL, databaseProperty.getDatabaseURL());
        settings.put(USER, databaseProperty.getDatabaseUser());
        settings.put(PASS, databaseProperty.getDatabasePassword());
        settings.put(DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(HBM2DDL_AUTO, databaseProperty.getDatabaseHBM2DLL());
        settings.put(SHOW_SQL, databaseProperty.getDataBaseShowSql());
        settings.put(DEFAULT_SCHEMA, databaseProperty.getDataBaseSchema());
        settings.put(USE_SECOND_LEVEL_CACHE, databaseProperty.getUseSecondCache());
        settings.put(USE_QUERY_CACHE, databaseProperty.getUseQueryCache());
        settings.put(USE_MINIMAL_PUTS, databaseProperty.getUseMinimalPuts());
        settings.put(CACHE_REGION_PREFIX, databaseProperty.getUseRegionPrefix());
        settings.put(CACHE_REGION_FACTORY, databaseProperty.getFactoryClass());
        settings.put(CACHE_PROVIDER_CONFIG, databaseProperty.getHZConfFile());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(User.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    private void initAdminUser() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final UserDTORepository userDTORepository = new UserDTORepositoryImpl(entityManager);
        if (userDTORepository.findByLogin("admin") == null) {
            try {
                entityManager.getTransaction().begin();
                userDTORepository.save(new UserDTO("admin", Role.ADMIN, "5759bb213e44ab5d7527cb0c29ffb911"));
                entityManager.getTransaction().commit();
                System.out.println("Admin user with password `admin` initialised");
            } catch (@NotNull final Exception e) {
                entityManager.getTransaction().rollback();
                throw e;
            } finally {
                entityManager.close();
            }
        }
    }

}

package ru.t1.aayakovlev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface ExtendedRepository<E extends AbstractUserOwnedModel> extends BaseRepository<E> {

    @NotNull
    E save(@NotNull final String userId, @NotNull final E model);

    int count(@NotNull final String userId);

    void clear(@NotNull final String userId);

    @NotNull
    List<E> findAll(@NotNull final String userId);

    @NotNull
    List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator);

    @Nullable
    E findById(@NotNull final String userId, @NotNull final String id);

    void removeById(@NotNull final String userId, @NotNull final String id);

    @NotNull
    E update(@NotNull final String userId, @NotNull E model);

}

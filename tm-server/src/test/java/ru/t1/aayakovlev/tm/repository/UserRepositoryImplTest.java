package ru.t1.aayakovlev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.repository.dto.UserDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.impl.UserDTORepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.dto.UserDTOService;
import ru.t1.aayakovlev.tm.service.dto.impl.UserDTOServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;

@Category(UnitCategory.class)
public final class UserRepositoryImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final UserDTORepository repository = new UserDTORepositoryImpl(connectionService.getEntityManager());

    @NotNull
    private static final UserDTOService service = new UserDTOServiceImpl(connectionService, propertyService);

    @Before
    public void init() throws AbstractException {
        service.save(COMMON_USER_ONE);
        service.save(COMMON_USER_TWO);
    }

    @After
    public void after() throws AbstractException {
        service.clear();
    }

    @Test
    public void When_FindByIdExistsUser_Expect_ReturnUser() {
        @Nullable final UserDTO user = repository.findById(COMMON_USER_ONE.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(COMMON_USER_ONE.getEmail(), user.getEmail());
        Assert.assertEquals(COMMON_USER_ONE.getFirstName(), user.getFirstName());
        Assert.assertEquals(COMMON_USER_ONE.getLastName(), user.getLastName());
        Assert.assertEquals(COMMON_USER_ONE.getMiddleName(), user.getMiddleName());
        Assert.assertEquals(COMMON_USER_ONE.getLogin(), user.getLogin());
    }

    @Test
    public void When_FindByIdExistsUser_Expect_ReturnNull() {
        @Nullable final UserDTO user = repository.findById(USER_ID_NOT_EXISTED);
        Assert.assertNull(user);
    }

    @Test
    @SneakyThrows
    public void When_SaveNotNullUser_Expect_ReturnUser() {
        repository.save(ADMIN_USER_THREE);
        @Nullable final UserDTO user = repository.findById(ADMIN_USER_THREE.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_USER_THREE.getId(), user.getId());
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListUsers() {
        final List<UserDTO> users = repository.findAll();
        for (int i = 0; i < users.size(); i++) {
            Assert.assertEquals(COMMON_USER_LIST.get(i).getId(), users.get(i).getId());
        }
    }

    @Test
    @SneakyThrows
    public void When_RemoveExistedUser_Expect_ReturnUser() {
        repository.save(ADMIN_USER_ONE);
        repository.removeById(ADMIN_USER_ONE.getId());
        Assert.assertNull(repository.findById(ADMIN_USER_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void When_RemoveAll_Expect_NullUsers() {
        repository.save(ADMIN_USER_ONE);
        repository.save(ADMIN_USER_TWO);
        repository.clear();
        Assert.assertNull(repository.findById(ADMIN_USER_ONE.getId()));
        Assert.assertNull(repository.findById(ADMIN_USER_TWO.getId()));
    }

    @Test
    @SneakyThrows
    public void When_RemoveByIdExistedUser_Expect_User() {
        repository.save(ADMIN_USER_ONE);
        repository.removeById(ADMIN_USER_ONE.getId());
        Assert.assertNull(repository.findById(ADMIN_USER_ONE.getId()));
    }

    @Test
    public void When_RemoveByIdNotExistedUser_Expect_ThrowsEntityNotFoundException() {
        repository.removeById(USER_ID_NOT_EXISTED);
    }

}

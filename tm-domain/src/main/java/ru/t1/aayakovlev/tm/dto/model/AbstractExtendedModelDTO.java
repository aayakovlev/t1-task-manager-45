package ru.t1.aayakovlev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractExtendedModelDTO extends AbstractModelDTO {

    @Nullable
    @Column(name = "user_id", columnDefinition = "VARCHAR(36)")
    private String userId;

}
